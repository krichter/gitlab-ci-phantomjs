package richtercloud.gitlab.ci.phantomjs;

import com.google.common.io.Files;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.io.FileUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author richter
 */
@RunWith(Arquillian.class)
public class GitLabCIPhantomJSIT {
    private final static Logger LOGGER = LoggerFactory.getLogger(GitLabCIPhantomJSIT.class);
    private static final String WEBAPP_SRC = new File(System.getProperty("basedir"),
            "src/main/webapp").getPath();
        //placing relative to basedir is necessary in order to be able to run
        //Maven from parent, aggregator and project itself
    private final static String SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME = "screenshotOutputMode";
    private final static String SCREENSHOT_OUTPUT_MODE_FILE = "file";
    private final static String SCREENSHOT_OUTPUT_MODE_BASE64 = "base64";

    /**
     * The deployment creation as needed by Arquillian.
     *
     * Resolves the {@code richtercloud:project1-jar:jar} Maven artifact and
     * manipulates the contained {@code persistence.xml} in order to access the
     * test database by unpacking and repacking the archive.
     *
     * @return the configured deployment archive
     */
    /*
    internal implementation notes:
    - This should work, but doesn't (combinations of different Java EE servers
    in different modes (embedded, remote, etc.) and ways to create ShrinkWrap
    archives - all with completely useless feedback, of course - are endless),
    asked https://stackoverflow.com/questions/46680947/illegalstateexception-could-not-find-backup-for-factory-javax-faces-context-fa
    for input on what appears to be the easiest approach
    */
    @Deployment(testable = false)
    public static Archive<?> createDeployment0() throws TransformerException, XPathExpressionException, ParserConfigurationException, SAXException, IOException, Exception {
        WebArchive retValue = ShrinkWrap.create(WebArchive.class)
                .add(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebResource(new File(WEBAPP_SRC, "index.xhtml"))
                .addAsWebResource(new File(WEBAPP_SRC, "login.xhtml"))
                .addAsWebInfResource(
                        new StringAsset("<faces-config version=\"2.0\"/>"),
                        "faces-config.xml");
        ByteArrayOutputStream archiveContentOutputStream = new ByteArrayOutputStream();
        retValue.writeTo(archiveContentOutputStream, Formatters.VERBOSE);
        LOGGER.info(archiveContentOutputStream.toString());
        return retValue;
    }

    private final File screenshotDir;
    private int screenshotCounter = 0;

    @Drone
    private WebDriver browser;
    @ArquillianResource
    private URL deploymentUrl;
    @FindBy(id = "mainForm:inputText")
    private WebElement inputText;
    @FindBy(id="loginForm:loginButton")
    private WebElement loginButton;

    public GitLabCIPhantomJSIT() {
        this.screenshotDir = Files.createTempDir();
        LOGGER.info(String.format("screenshot directory is '%s'",
                screenshotDir.getAbsolutePath()));
    }

    @Test
    public void testSomething() throws IOException {
        browser.get(deploymentUrl+"index.xhtml");
        screenshot();
        logPageSource();
        inputText.sendKeys("abc");
        screenshot();
//        Assert.assertEquals("abc",
//                inputText.getText());
        browser.get(deploymentUrl+"login.xhtml");
        screenshot();
        logPageSource();
        Assert.assertTrue(loginButton.isDisplayed());
    }

    private void screenshot() throws IOException {
        String screenshotOutputMode = System.getProperty(SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME,
                SCREENSHOT_OUTPUT_MODE_FILE);
        assert screenshotOutputMode != null;
        switch(screenshotOutputMode) {
            case SCREENSHOT_OUTPUT_MODE_FILE:
                File scrFile = ((TakesScreenshot)browser).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File(screenshotDir, String.valueOf(screenshotCounter)));
                break;
            case SCREENSHOT_OUTPUT_MODE_BASE64:
                String screenshotBase64 = ((TakesScreenshot)browser).getScreenshotAs(OutputType.BASE64);
                LOGGER.info(String.format("base64 encoding of screenshot: %s",
                        screenshotBase64));
                break;
            default:
                throw new IllegalArgumentException(String.format("Illegal "
                        + "value '%s' specified for property %s",
                        screenshotOutputMode,
                        SCREENSHOT_OUTPUT_MODE_PROPERTY_NAME));
        }
        screenshotCounter++;
    }

    private void logPageSource() {
        LOGGER.debug(String.format("===\nbrowser page source:\n===\n%s\n===",
                browser.getPageSource()));
    }
}
